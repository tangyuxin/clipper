import Vue from 'vue'
import Router from 'vue-router'
import clipper from './maps/clipper'
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/clipper',
      components: {
        default: (resolve) => require(['@/components/system/default'], resolve)
      },
      children: [
        ...clipper
      ]
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  } else {
    document.title = '商品素材-抓取工具'
  }
  next()
})
export default router
