export default [
  {
    path: 'index',
    name: 'itemClipper',
    component (resolve) {
      require(['@/pages/clipper/index'], resolve)
    },
    meta: {
      title: '商品素材-抓取工具'
    }
  },
  {
    path: 'main',
    name: 'clipperMain',
    component (resolve) {
      require(['@/pages/clipper/main'], resolve)
    },
    meta: {
      title: '商品素材-抓取工具'
    }
  }
]
