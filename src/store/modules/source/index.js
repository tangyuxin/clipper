export default {
  namespaced: true,
  state: {
    source: ''
  },
  mutations: {
    setSource (state, data) {
      state.source = data
    }
  },
  actions: {}
}
