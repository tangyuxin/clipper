import Vue from 'vue'
import Vuex from 'vuex'

import source from './modules/source/index'

Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    source
  }
})
