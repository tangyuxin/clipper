/**
 * 异步请求 用await来获取结果，严格模式，返回结果不为1则执行异常抛出
 */
import Api from '@/util/api'

let asyncRequestStrict = (url, data = {}) => {
  if (!url || url.length <= 0) {
    return
  }
  return new Promise((resolve, reject) => {
    Api.get({
      url: url,
      data: {
        ...data
      },
      success (res) {
        resolve(res)
      },
      fail (res) {
        reject(res)
      }
    })
  })
}

let dateFormat = (fmt, dated) => {
  let date = new Date(dated)
  let o = {
    'Y+': date.getFullYear(),
    'M+': date.getMonth() + 1, // 月份
    'd+': date.getDate(), // 日
    'h+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
    }
  }
  return fmt
}
export default {
  asyncRequestStrict,
  dateFormat
}
