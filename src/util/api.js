/**
 * Created by Administrator on 2017/6/15.
 */

import Vue from 'vue'

export default {
  api_url: 'https://tangyuxin.top/api/',

  get: function (obj, url) {
    this.request(obj, url || this.api_url)
  },
  /**
   * 实际网络请求
   * 1. API是否需要登陆，默认需要登陆才可以访问，未登陆，自动跳转登陆链接
   * 2. 如果已登陆，存储凭证数据
   */
  request: function (obj, url) {
    if (obj.data === undefined) {
      obj.data = {}
    }
    let params = {
      params: obj.data
    }
    if (obj.url !== undefined) {
      url = url + obj.url
    }
    let api = Vue.http.get(url, params)
    api.then(function (response) {
      let res = response.body
      if (!isNaN(res.state)) { // 状态码强制转为整型
        res.state = Number(res.state)
      }
      typeof obj.success === 'function' && obj.success(res)
    }, function (response) {
      typeof obj.fail === 'function' && obj.fail(response)
    }).catch(() => {})
  }
}
